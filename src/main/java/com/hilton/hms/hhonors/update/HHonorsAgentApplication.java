package com.hilton.hms.hhonors.update;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author jon
 *
 * The entry point to the application. 
 * 
 * java -Dproperties.location=classpath:/hhonors-agent.properties -jar ./target/hms-hhonors-agent-0.1.0.jar
 */
@Configuration
@ImportResource ("classpath:/hhonors-agent-context.xml")
public class HHonorsAgentApplication {
    
    /*
     * public constructor required for app startup
     */
    @SuppressWarnings("all")
    public HHonorsAgentApplication() {
        //
    }

    public static void main (String[] args) throws InterruptedException {
        SpringApplication.run (HHonorsAgentApplication.class, args);
    }
}
