package com.hilton.hms.hhonors.update.service.impl;

import java.nio.charset.Charset;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.hilton.hms.hhonors.update.model.HHonorsUpdate;
import com.hilton.hms.hhonors.update.service.MessageRulesMatchingService;
import com.hilton.hms.util.security.HMSSecurityUtils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class HHonorsUpdateMessageRulesMatchingService implements MessageRulesMatchingService<HHonorsUpdate> {
    private static final transient Log LOG = LogFactory.getLog(HHonorsUpdateMessageRulesMatchingService.class);

    private static final String MATCH_MSG = " matched and is queued for further processing.";
    private static final String NO_MATCH_MSG = " did not match and is ignored.";
    private static final String DISCARD_MSG = "Message has no hhonors id, discarding.";
    private static final String INBOUND_MSG = "Inbound message from queue for hhonorsID: ";
    private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    
    @Autowired
    JedisPool jedisPool;

    @Value("${redis.hhonorsPassbook}")
    String redisHhonorsPassbook;

    @Value("${passbook.security.serial.salt}")
    private String serialSalt;
    
    @Override
    public HHonorsUpdate read(HHonorsUpdate message) {
        String hhonorsID = message.getHhonorsID();
        if (StringUtils.isBlank(hhonorsID)) {
            LOG.error(DISCARD_MSG);
            return null;
        }
        
        LOG.info(INBOUND_MSG + hhonorsID);
        
        Jedis jedis = jedisPool.getResource();
        try {
            // matching -- do we know this member
            String token = generateSerialNumber(hhonorsID, serialSalt);
            boolean knownMember = jedis.sismember(redisHhonorsPassbook, token);
            if (knownMember) {
                LOG.debug(hhonorsID + MATCH_MSG);
                message.setToken(token);
                return message;
            }
            else {
                LOG.debug(hhonorsID + NO_MATCH_MSG);
                return null;
            }
        }
        catch (JedisConnectionException e) {
            LOG.error("Jedis connection error: " + ExceptionUtils.getStackTrace(e), e);
            // can't recover
            if (null != jedis) {
                jedisPool.returnResource(jedis);
            }
        }
        finally {
            if (null != jedis) {
                jedisPool.returnResource(jedis);
            }
        }
        return null;
    }

    public static String generateSerialNumber(String hhonorsID, String serialSalt) {
        StringBuilder sb = new StringBuilder();
        sb.append(hhonorsID).append(":").append(serialSalt);
        return HMSSecurityUtils.toSHA1(sb.toString().getBytes(DEFAULT_CHARSET));
    }    
}
