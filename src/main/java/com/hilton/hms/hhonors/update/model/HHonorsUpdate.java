package com.hilton.hms.hhonors.update.model;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * @author jon
 *
 * Represents the message queued from the back end hilton systems
 */


@JsonPropertyOrder({
    "hhonorsID",
    "tier",
    "points",
})

public class HHonorsUpdate {
    
    @JsonProperty("hhonorsID")
    private String hhonorsID;
    
    @JsonProperty("tier")
    private String tier;
    
    @JsonProperty("points")
    private Long points;
    
    private String token;
    
    @JsonProperty("hhonorsID")
    public String getHhonorsID() {
        return hhonorsID;
    }

    @JsonProperty("hhonorsID")
    public void setHhonorsID(String hhonorsID) {
        this.hhonorsID = hhonorsID;
    }

    @JsonProperty("tier")
    public String getTier() {
        return tier;
    }

    @JsonProperty("tier")
    public void setTier(String tier) {
        this.tier = tier;
    }

    @JsonProperty("points")
    public Long getPoints() {
        return points;
    }

    @JsonProperty("points")
    public void setPoints(Long points) {
        this.points = points;
    }
    
    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
}
