package com.hilton.hms.hhonors.update.service;

/*
 * Interface for MessageMatchingRulesEngine.
 * 
 * Implementations are hooked into a channel as a service activator.
 * 
 * An implementation will receive messages via read, and return a message
 * to be passed on or return null if discarding. 
 */
public interface MessageRulesMatchingService<T> {
    T read(T message);
}
