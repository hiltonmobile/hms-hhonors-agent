package com.hilton.hms.hhonors.update;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import com.hilton.hms.hhonors.update.model.HHonorsUpdate;
import com.hilton.hms.test.AbstractHMSTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration ("classpath:/test-hhonors-agent-context.xml")
public class HHonorsAgentIT extends AbstractHMSTest {
    private static final transient Log LOG = LogFactory.getLog(HHonorsAgentIT.class);

    @Autowired
    JedisPool jedisPool;
    
    @Autowired 
    private AmqpTemplate template;
    
    @Value("${queue.hhonorsUpdate}")
    private String hiltonInboundQueue;
    
    @Test
    public void testJedisConnection() {
        Jedis jedis = jedisPool.getResource();
        String testKey = "hello";
        String testVal = "world";
        
        try {
            jedis.set(testKey, testVal);
            String result = jedis.get("hello");
            LOG.debug("Jedis response for get \"hello\": " + result);
            LOG.info("yo");
            assertTrue("Expected world", testVal.equals(result));
        }
        catch (JedisConnectionException e) {
            // can't recover
            if (null != jedis) {
                jedisPool.returnResource(jedis);
            }
        }
        finally {
            if (null != jedis) {
                jedisPool.returnResource(jedis);
            }
        }
    }

// TODO move this to a unit test    
//    @Test
//    public void test01_synchronouslyQueueAndReceiveTestMessage() {
//        String hhonorsID = "205587078";
//        long points = 10000l;
//        String tier = "BLUE";
//        
//        LOG.debug("Queue is: " + "someTestQueue");
//        try {
//            HHonorsUpdate sendUpdate = new HHonorsUpdate();
//            sendUpdate.setHhonorsID(hhonorsID);
//            sendUpdate.setPoints(points);
//            sendUpdate.setTier(tier);
//            template.convertAndSend(hiltonInboundQueue, sendUpdate);
//            LOG.debug("queued update for hhonorsID: " + hhonorsID);
//            
//            HHonorsUpdate receivedUpdate = (HHonorsUpdate)template.receiveAndConvert(hiltonInboundQueue);
//        }
//        catch (AmqpException e) {
//            LOG.debug(ExceptionUtils.getStackTrace(e));
//            fail("amqp exception " + e.getMessage());
//        }
//    }
    

    @Test
    public void test02_queueTestMessages() {

        for (int i = 0; i < 10; i++) {
            String hhonorsID = "205587078";
            String tier = "BLUE";
            long points = 10000l;
            
            HHonorsUpdate sendUpdate = new HHonorsUpdate();
            sendUpdate.setHhonorsID(hhonorsID);
            sendUpdate.setPoints(10000l);
            sendUpdate.setTier("BLUE");
            
            ObjectMapper mapper = new ObjectMapper();
            String json;
            
            try {
                json = mapper.writeValueAsString(sendUpdate);
                template.convertAndSend(hiltonInboundQueue, json);
                LOG.debug("queued update for " + hhonorsID);
            } catch (Exception e) {
                LOG.error("problem mapping object to json " + e.getMessage() + " " + ExceptionUtils.getStackTrace(e));
                fail(e.getMessage());
            }
            
            hhonorsID = "205587000";
            sendUpdate.setHhonorsID(hhonorsID);
    
            try {
                json = mapper.writeValueAsString(sendUpdate);
                template.convertAndSend(hiltonInboundQueue, json);
                LOG.debug("queued update for " + hhonorsID);
            } catch (Exception e) {
                LOG.error("problem mapping object to json " + e.getMessage() + " " + ExceptionUtils.getStackTrace(e));
                fail(e.getMessage());
            }
        }
    }
    
    @Test
    public void testMapper() throws JsonGenerationException, JsonMappingException, IOException {
        String hhonorsID = "205587078";
        String tier = "BLUE";
        long points = 1000l;
        
        HHonorsUpdate update = new HHonorsUpdate();
        update.setHhonorsID(hhonorsID);
        update.setPoints(points);
        update.setTier(tier);
        
        ObjectMapper mapper = new ObjectMapper();
        LOG.info(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(update));
        
        String json = mapper.writeValueAsString(update);
        HHonorsUpdate jsonUpdate = mapper.readValue(json, HHonorsUpdate.class);
        LOG.info(jsonUpdate.getHhonorsID() + " " + jsonUpdate.getTier() + " " + jsonUpdate.getPoints() + " " + jsonUpdate.getToken());
    }
}
