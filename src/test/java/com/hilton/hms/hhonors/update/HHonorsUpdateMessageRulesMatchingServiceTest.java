package com.hilton.hms.hhonors.update;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import org.jmock.Expectations;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import com.hilton.hms.hhonors.update.model.HHonorsUpdate;
import com.hilton.hms.hhonors.update.service.impl.HHonorsUpdateMessageRulesMatchingService;
import com.hilton.hms.test.AbstractHMSTest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration ("classpath:/test-hhonors-agent-context.xml")
public class HHonorsUpdateMessageRulesMatchingServiceTest extends AbstractHMSTest {

    @Autowired
    HHonorsUpdateMessageRulesMatchingService service;
    
    @Value("${redis.hhonorsPassbook}")
    String redisHhonorsPassbook;

    @Value("${passbook.security.serial.salt}")
    private String serialSalt;
    
    @Test 
    public void test01_empty_read() {
        // empty hhonors, read will return null
        HHonorsUpdate update = new HHonorsUpdate();
        HHonorsUpdate processed = service.read(update);
        assertNull(processed);
    }

    @Test 
    public void test02_empty_read() {
        // empty hhonors, read will return null
        HHonorsUpdate update = new HHonorsUpdate();
        update.setHhonorsID("");
        HHonorsUpdate processed = service.read(update);
        assertNull(processed);
    }
    
    @Test
    public void test03_match_read() {
        // hhonorsid matches redis, read returns message and token is now set
        HHonorsUpdate update = new HHonorsUpdate();
        String testHhonorsID = AbstractHMSTest.randomString("0123456789", 9);
        final String serial = HHonorsUpdateMessageRulesMatchingService.generateSerialNumber(testHhonorsID, serialSalt);
        
        final JedisPool mockJedisPool = mock(JedisPool.class);
        inject(service, "jedisPool", mockJedisPool);

        final Jedis mockJedis = mock(Jedis.class);
        
        expect(new Expectations(){{
            oneOf(mockJedisPool).getResource(); will(returnValue(mockJedis));
            oneOf(mockJedis).sismember(redisHhonorsPassbook, serial);will(returnValue(true));
            oneOf(mockJedisPool).returnResource(mockJedis);
        }});
        
        update.setHhonorsID(testHhonorsID);
        HHonorsUpdate processed = service.read(update);
        assertNotNull(processed);
        assertNotNull(processed.getHhonorsID());
    }

    @Test
    public void test04_nomatch_read() {
        // hhonorsid does not match redis, read returns null
        HHonorsUpdate update = new HHonorsUpdate();
        String testHhonorsID = AbstractHMSTest.randomString("0123456789", 9);
        final String serial = HHonorsUpdateMessageRulesMatchingService.generateSerialNumber(testHhonorsID, serialSalt);
        
        final JedisPool mockJedisPool = mock(JedisPool.class);
        inject(service, "jedisPool", mockJedisPool);

        final Jedis mockJedis = mock(Jedis.class);
        
        expect(new Expectations(){{
            oneOf(mockJedisPool).getResource(); will(returnValue(mockJedis));
            oneOf(mockJedis).sismember(redisHhonorsPassbook, serial);will(returnValue(false));
            oneOf(mockJedisPool).returnResource(mockJedis);
        }});
        
        update.setHhonorsID(testHhonorsID);
        HHonorsUpdate processed = service.read(update);
        assertNull(processed);
    }
    
    @Test
    public void test05_jedisException_read() {
        // hhonorsid does not match redis, read returns null
        HHonorsUpdate update = new HHonorsUpdate();
        String testHhonorsID = AbstractHMSTest.randomString("0123456789", 9);
        final String serial = HHonorsUpdateMessageRulesMatchingService.generateSerialNumber(testHhonorsID, serialSalt);
        
        final JedisPool mockJedisPool = mock(JedisPool.class);
        inject(service, "jedisPool", mockJedisPool);

        final Jedis mockJedis = mock(Jedis.class);
        
        expect(new Expectations(){{
            oneOf(mockJedisPool).getResource(); will(returnValue(mockJedis));
            oneOf(mockJedis).sismember(redisHhonorsPassbook, serial);will(throwException(new JedisConnectionException("")));
            allowing(mockJedisPool).returnResource(mockJedis);
        }});
        
        update.setHhonorsID(testHhonorsID);
        HHonorsUpdate processed = service.read(update);
        assertNull(processed);
    }
}
